import './App.scss'
import {Router} from './router'
import { useEffect, useState } from 'react'
import { useFetch } from './hooks'
import { modalProps } from "./components/modal/modalProps"
import  Modal  from "./components/modal/index"
import  Header  from "./components/header/index"

function App () {
    const [cart, setCart] = useState(JSON.parse(localStorage.getItem('cart')) || [])
    const [favorites, setFavorites] = useState(JSON.parse(localStorage.getItem('favorites')) || [])
    const [article, setArticle] = useState(null)
    const [isOpen, setIsOpen] = useState(null)

    const totalQuantity = cart.reduce((sum, current) => sum + current.quantity, 0);
    const [cartItemCount, setCartItemCount] = useState(totalQuantity)
    
    const {data: products, loading} = useFetch('/products.json');
      
    localStorage.setItem('products', JSON.stringify(products || []))
     
      useEffect(() => {
        if (Array.isArray(favorites)) {
          localStorage.setItem('favorites', JSON.stringify(favorites));
        } else {
          console.error('Favorites should be an array');
        }
      
        if (Array.isArray(cart)) {
          localStorage.setItem('cart', JSON.stringify(cart));
          localStorage.setItem('cartQuantity', JSON.stringify(totalQuantity));
          setCartItemCount(totalQuantity);
        } else {
          console.error('Cart should be an array');
        }
      }, [favorites, cart, totalQuantity]);
      

      //return object with modal data
      const findModalType = () => {
        return modalProps.find(modal => modal.type === isOpen); 
      }
     
      const openModal = (e, type, article) =>{
        e.preventDefault()
        setIsOpen(type)
        setArticle(article)
      };
      
      const closeModal= () => {
        setIsOpen(null)
        setArticle(null)
      }
      //add product to favorites list
      const addToFavList = (e, article) => {
        e.preventDefault()

        if (!favorites.includes(article)) {
          setFavorites([...favorites, article]); 
      } else {
        const newFavorites = favorites.filter(fav => fav !== article)
        setFavorites(newFavorites)
      }
    }
    //add product to cart
      const addToCart = () =>{
        const newCartItem = products.find(product => product.article === article);
        const itemInCartIndex = cart.findIndex(item => item.id === newCartItem.article);
      
        if (itemInCartIndex === -1) {
          // add new item to cart
          const newItem = { id: newCartItem.article, quantity: 1 };
          setCart(prevCart => [...prevCart, newItem]);
        } else {
          // increment item quantity in cart
          const updatedItem = { ...cart[itemInCartIndex], quantity: cart[itemInCartIndex].quantity + 1 };
          const updatedCart = [...cart.slice(0, itemInCartIndex), updatedItem, ...cart.slice(itemInCartIndex + 1)];
          setCart(updatedCart);
        }
        setIsOpen(null);
      };

      //increase the quantity of an item in the cart by 1 (+)
      const increaseCartItem = (article) => {
        const itemInCartIndex = cart.findIndex(item => item.id === article);
        const updatedItem = { ...cart[itemInCartIndex], quantity: cart[itemInCartIndex].quantity + 1 };
        const updatedCart = [...cart.slice(0, itemInCartIndex), updatedItem, ...cart.slice(itemInCartIndex + 1)];
        setCart(updatedCart)
      };

      //remove product from the card
      const removeFromCart = () =>{
        const delItemIndex = cart.findIndex(item => item.id === article)
        setCart([...cart.slice(0, delItemIndex), ...cart.slice(delItemIndex + 1)])
        closeModal()
      }

      //remove a single item from the cart
      const removeSingleItemFromCart = (article) => {
        const itemIndex = cart.findIndex(item => item.id === article);
        if (itemIndex !== -1) {
          const item = cart[itemIndex];
          if (item.quantity > 1) {
            const updatedItem = {...item, quantity: item.quantity - 1};
            setCart([...cart.slice(0, itemIndex), updatedItem, ...cart.slice(itemIndex + 1)]);
          } else {
            setCart([...cart.slice(0, itemIndex), ...cart.slice(itemIndex + 1)]);
          }
        }
      }
      const routerProps = {
        loading: loading,
        cart: cart,
        products: products,
        favorites: favorites,
        addToFavList: addToFavList, 
        openModal: openModal,
        increaseCartItem:increaseCartItem,
        removeSingleItemFromCart: removeSingleItemFromCart
      };
return (<>
    {isOpen 
        && <Modal 
        data = { findModalType() }
        closeModal = { closeModal }
        addToCart = { addToCart } 
        removeFromCart = { removeFromCart }
        closeButton = { findModalType().closeButton }/> }
   
           <Header 
           favorites={favorites.length}
           cart={cartItemCount}/>
           
    <Router {...routerProps}/>
    </>
)
} 

export default App

