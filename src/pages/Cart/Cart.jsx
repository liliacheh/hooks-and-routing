import ProductList from "../../components/productList"
import { Loader } from "../../components/loader";
import PropTypes from 'prop-types';


export function Cart(props){
    const {loading,products, addToFavList, openModal, cart, increaseCartItem, favorites, removeSingleItemFromCart} = props
    const cartIds = cart.map(item => item.id);
    const cartProducts = products.filter(product => cartIds.includes(product.article));
  
    return (
        <>
        {!loading ?
            cartProducts.length ?
            <ProductList 
            products={cartProducts} 
            productNum={true}
            type='delete' 
            addToFavList={addToFavList} 
            openModal={openModal}
            increaseCartItem={increaseCartItem}
            favorites={favorites}
            removeSingleItemFromCart={removeSingleItemFromCart}
            cart={cart}/>
        : <p>You have not added anything to the cart yet</p>
        : <Loader/>
        }
        </>
        

    )
}
Cart.propTypes = {
    loading: PropTypes.bool,
    products: PropTypes.array,
    cart: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number,
        quantity: PropTypes.number
    })),
    addToFavList: PropTypes.func,
    openModal: PropTypes.func,
    increaseCartItem: PropTypes.func,
    favorites: PropTypes.arrayOf(PropTypes.number),
    removeSingleItemFromCart: PropTypes.func,
}