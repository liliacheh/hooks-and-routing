import ProductList from "../../components/productList";
import { Loader } from "../../components/loader";
import PropTypes from 'prop-types';


export default function Home (props){
const {loading, products, addToFavList, openModal, cart, increaseCartItem, favorites, removeSingleItemFromCart } = props
  
      return(
     <>
     {!loading ?
     <ProductList 
     products={products}
     type='buy'
     addToFavList={addToFavList}
     openModal={openModal}
     cart={cart}
     increaseCartItem={increaseCartItem}
     favorites={favorites}
     removeSingleItemFromCart={removeSingleItemFromCart}
     />
     :
     <Loader/>}
     </>
      )
  };

Home.propTypes = {
    loading: PropTypes.bool,
    products: PropTypes.array,
    cart: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number,
        quantity: PropTypes.number
    })),
    addToFavList: PropTypes.func,
    openModal: PropTypes.func,
    increaseCartItem: PropTypes.func,
    favorites: PropTypes.arrayOf(PropTypes.number),
    removeSingleItemFromCart: PropTypes.func,
}