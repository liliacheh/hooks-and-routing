import ProductList from "../../components/productList";
import { Loader } from "../../components/loader";
import PropTypes from 'prop-types';


export function Favorites(props){
    const {favorites, loading, products, addToFavList, openModal, cart, increaseCartItem, removeSingleItemFromCart} = props
    const favProducts = products.filter(product => favorites.includes(product.article) )
    
    return (<> 
    {!loading ?
    favProducts.length ? 
        <ProductList 
        products={favProducts} 
        type='buy'
        addToFavList={addToFavList}
        openModal={openModal}
        cart={cart}
        increaseCartItem={increaseCartItem}
        removeSingleItemFromCart={removeSingleItemFromCart}
        favorites={favorites}/> 
        : <p>You haven't added anything to your favorites yet</p>
        : <Loader/>}
        </>
    )
}

Favorites.propTypes = {
    loading: PropTypes.bool,
    products: PropTypes.array,
    cart: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number,
        quantity: PropTypes.number
    })),
    addToFavList: PropTypes.func,
    openModal: PropTypes.func,
    increaseCartItem: PropTypes.func,
    favorites: PropTypes.arrayOf(PropTypes.number),
    removeSingleItemFromCart: PropTypes.func,
}