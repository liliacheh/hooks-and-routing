import Home from '../pages/Home/home';
import { Favorites } from '../pages/Favorites/Favorites';
import { NotFound } from '../pages/NotFound/NotFound';
import { Cart } from '../pages/Cart/Cart';
import { Route, Routes } from "react-router-dom";

export function Router(props) {
  const {loading, cart, products, favorites, addToFavList, openModal, increaseCartItem, removeSingleItemFromCart} = props
  return (
    <Routes>
      <Route path='/' element = {<Home 
    loading = {loading} 
    products={products} 
    cart={cart} 
    favorites = {favorites} 
    addToFavList={addToFavList} 
    openModal={openModal}
    increaseCartItem={increaseCartItem}
    removeSingleItemFromCart={removeSingleItemFromCart}/>}/>
      <Route path='/favorites'element = {<Favorites 
    loading = {loading} 
    products={products} 
    cart={cart} 
    favorites = {favorites} 
    addToFavList={addToFavList} 
    openModal={openModal}
    increaseCartItem={increaseCartItem}
    removeSingleItemFromCart={removeSingleItemFromCart}/>}/>
      <Route path='/cart'element = {<Cart 
    loading = {loading} 
    products={products} 
    cart={cart} 
    favorites = {favorites} 
    addToFavList={addToFavList} 
    openModal={openModal}
    increaseCartItem={increaseCartItem}
    removeSingleItemFromCart={removeSingleItemFromCart}/>}/>
      <Route path='*' element = {<NotFound/>}/>

    </Routes>
  )
}
// import { createBrowserRouter } from 'react-router-dom'
// import Home from '../pages/Home/home'
// import { Favorites } from '../pages/Favorites/Favorites'
// import { NotFound } from '../pages/NotFound/NotFound'
// import { Cart } from '../pages/Cart/Cart'
// import { Layout } from '../pages/Layout/Layout'

// export const router = createBrowserRouter([
//     {
//         path : "/",
//         element : <Layout/>,
//         errorElement: <NotFound/>,
//         children: [
//             {
//                 element: <Home/>,
//                 index: true
//             },
//             {
//                 element: <Cart/>,
//                 path: '/cart',
//                 errorElement: <NotFound/>
//             },
//             {
//                 element: <Favorites />,
//                 path : "/favorites",
//                 errorElement: <NotFound/>
//             }
//         ] 
//     }
// ])


