import { FavIcon, CartIcon} from "../icons"
import style from "./header.module.scss"
import PropTypes from 'prop-types';
import { Link } from "react-router-dom";

export default function Header(props) {
 
    return (
      <>
        <header className={style.header}>
          <div className={style.header__container}>
            <div className={style.header__wrapper}>
              <Link to='/' className={style.header__logo}>
                <img className={style.header__img} src="./logo.svg" alt="logo" />
              </Link>
              <div className={style.header__btns}>
                <Link to='/favorites' className={style.header__favorites} title="Favorites">
                  <FavIcon />
                  {props.favorites > 0 && <div>{props.favorites}</div>}
                </Link>
                <Link to='/cart' className={style.header__cart} title="Cart">
                  <CartIcon />
                  {props.cart > 0 && <div>{props.cart}</div>}
                </Link>
              </div>
            </div>
          </div>
        </header>
      </>
    );
  }
 
Header.propTypes = {
    favorites: PropTypes.number,
    cart: PropTypes.number
}
Header.defaultProps = {
    favorites: 0,
    cart: 0
}
