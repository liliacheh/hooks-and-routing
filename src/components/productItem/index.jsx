import { useState, useEffect, useCallback} from "react"
import style from "./productItem.module.scss"
import Button from "../button";
import {FavIcon} from "../icons";
import PropTypes from 'prop-types';
import no_image from "../../img/no_image.png"


export default function ProductItem (props){
    const [isFavorite, setIsFavorite] = useState(false)
    
    const {product, type, productNum, addToFavList, openModal, cart, increaseCartItem, favorites, removeSingleItemFromCart} = props

    useEffect(()=> {
        favorites.includes(product.article) 
        && setIsFavorite(true)
    },[favorites, product.article])

    const countProduct = useCallback(() =>{
        const item = cart.find(item => item.id === product.article);
        return item ? item.quantity : 0;
    }, [cart, product.article])

    // const countProduct = () =>{
    //     const item = cart.find(item => item.id === product.article);
    //     return item ? item.quantity : 0;
    // } 
    const [productNumber, setProductNumber] = useState(countProduct())

    useEffect(() => {
        setProductNumber((countProduct()))
    }, [cart, countProduct])

    const toggleIsFav = ()=> {
        isFavorite ? setIsFavorite(false) : setIsFavorite(true)
    }

    //   const decreaseProductNumber = () => {
    //     if (productNumber > 1) {
    //       setProductNumber(productNumber - 1);
    //       props.removeSingleItemFromCart(product.article);
    //     }
    //   };    
    return(
            <a href='/' className={style.product}>
                <header className={style.product__header}>
                    <h3 className={style.product__name}>{product.name}</h3>
                  {type === 'delete'? 
                  <Button className={style.product__delBtn}
                  onClick={(e)=> {
                      openModal(e, 'delete', product.article)                                    
              }} title='Delete from cart'
                  >
                  </Button> : null}  
                </header>
                <div className={style.product__body}>
                    <div className={style.product__img}>
                        <img src={product.imgUrl || no_image} alt={product.name} />
                    </div>
                    <div className={style.product__descr}>
                    <div className={style.product__article}>{product.article}</div>
                        <div className={style.product__color}>{product.color}</div>
                        <div className={style.product__price}>{product.price} <span>₴</span>
                        </div>
                        {productNum && productNumber > 0
                            ?<div className={style.product__cartBtns}>
                            <Button className={style.product__minusBtn} onClick={(e) =>{
                                e.preventDefault()
                                removeSingleItemFromCart(product.article)
                            } } text={"-"}/>
                            <input className={style.product__itemQuantity}type="text" disabled value={countProduct()}/>
                            <Button className={style.product__plusBtn} onClick={(e) =>{
                                e.preventDefault()
                                increaseCartItem(product.article)}}
                                text={"+"}/>
                            </div>  : null}
                        <div className={style.product__btns}>
                            <Button className={style.product__favBtn}
                            onClick={(e)=> {   
                                addToFavList(e, product.article)                            
                                toggleIsFav()}}
                                title='Add to favorites'
                            ><FavIcon fill={isFavorite ? "black":"white"}/>
                            </Button>
                            {type ==='buy'?
                              <Button className={style.product__buyBtn}
                              onClick={(e)=> {
                                  openModal(e, 'buy', product.article)                                    
                          }}
                              text={"Add to cart"}>
                              </Button>
                              :  null}
                        </div>
                    </div>
                </div>
                        </a >
)
}
    

ProductItem.propTypes = {
    type: PropTypes.string,
    productNum : PropTypes.bool,
    product: PropTypes.shape({
        name: PropTypes.string,
        article: PropTypes.number,
        imgUrl: PropTypes.string,
        color: PropTypes.string,
        price: PropTypes.number,
    }),
    cart: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number,
        quantity: PropTypes.number
    })),
    addToFavList: PropTypes.func,
    openModal: PropTypes.func,
    increaseCartItem: PropTypes.func,
    favorites: PropTypes.arrayOf(PropTypes.number),
    removeSingleItemFromCart: PropTypes.func,
}

ProductItem.defaultProps = {
    product: {
        imgUrl: no_image,
    },
}