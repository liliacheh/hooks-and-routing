import style from "./productList.module.scss";
import ProductItem from "../productItem";
import PropTypes from 'prop-types';

export default function ProductList (props){
   
        const { products, type, productNum, addToFavList, openModal, cart, increaseCartItem, favorites, removeSingleItemFromCart } = props
        return(
            <div className={style.productList}>
            <div className={style.productList__container}>
                <div className={style.productList__wrapper}>
                    {products.map(product => 
                        <ProductItem
                            key={product.article}
                            product={product}
                            productNum={productNum}
                            type={type}
                            addToFavList={addToFavList}
                            openModal={openModal}
                            cart={cart}
                            increaseCartItem={increaseCartItem}
                            favorites={favorites}
                            removeSingleItemFromCart={removeSingleItemFromCart}
                            /> 
                    )}
                </div>
            </div>
        </div>
        )
}


ProductList.propTypes = {
    product: PropTypes.array,
    productNum: PropTypes.bool,
    type: PropTypes.string,
    cart: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number,
        quantity: PropTypes.number
    })),
    addToFavList: PropTypes.func,
    openModal: PropTypes.func,
    increaseCartItem: PropTypes.func,
    favorites: PropTypes.arrayOf(PropTypes.number),
    removeSingleItemFromCart: PropTypes.func,
}
