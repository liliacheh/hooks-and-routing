export const modalProps = [
{
    type: 'buy',
    header: "Add product to cart?",
    closeButton: true,
    text: <><p>Are you sure you want to add this item to your cart?</p></>,
    
    closeBtn(className, closeHandler ){
        return(
        <button className={className} onClick={closeHandler}></button>
    )
},
    actions(className, addToCart, cancel){
        return(
        <>
 <button className={className} onClick={addToCart}>Add to cart</button>
 <button className={className} onClick={cancel}>Cancel</button>
        </>
    )
}
},
{
    type: 'delete',
    header: "Delete product from cart?",
    closeButton: false,
    text: <><p>Are you sure you want to delete this item from your cart?</p></>,
    
    closeBtn(className, closeHandler ){
        return(
        <button className={className} onClick={closeHandler}></button>
    )
},
    actions(className, removeFromCart, cancel){
        return(
        <>
 <button className={className} onClick={removeFromCart}>Delete</button>
 <button className={className} onClick={cancel}>Cancel</button>
        </>
    )
}
}
]