import React, { useEffect } from 'react';
import style from "./modal.module.scss"
import PropTypes from 'prop-types'

export default function Modal (props){
 
    useEffect(() => {
        function handleClickOutside(e) {
          const modal = document.querySelector("#modal__body");
          if (modal && !modal.contains(e.target)) {
            props.closeModal();
          }
        }
        document.addEventListener("mousedown", handleClickOutside);
        return () => {
          document.removeEventListener("mousedown", handleClickOutside);
        };
      }, [props]);

    const modalProps = props.data
    
        return(
            <div className={style.modal}>
                <div className={style.modal__body} id="modal__body">
                    <div className={style.modal__header}>
                    <h3 className={style.modal__title}>{modalProps.header}</h3>
                    {props.closeButton && modalProps.closeBtn(style.modal__close, props.closeModal)}
                    </div>
                    <div className={style.modal__text}>
                    {modalProps.text}
                    </div>
                    <div className={style.modal__btns}>
                    {modalProps.actions && modalProps.type === 'buy' ?
                    modalProps.actions(style.modal__btn,props.addToCart, props.closeModal)
                    :modalProps.actions(style.modal__btn,props.removeFromCart, props.closeModal)}

                  </div>
                </div>
            </div>
        )
    
}
Modal.propTypes = {
    closeButton: PropTypes.bool,
    closeModal: PropTypes.func,
    addToCart: PropTypes.func,
    removeFromCart: PropTypes.func, 
    data: PropTypes.shape({
        type: PropTypes.string,
        header: PropTypes.string,
        text: PropTypes.object,
        closeBtn: PropTypes.func,
        actions: PropTypes.func,
    })
    


}
